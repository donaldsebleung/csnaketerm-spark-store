# csnaketerm-spark-store

## 介绍

[csnaketerm](https://github.com/DonaldKellett/csnaketerm) 经典终端贪食蛇游戏 deb 包，供[星火应用商店](https://spark-app.store/)上架

## 使用说明

于仓库根目录执行以下命令：

```bash
$ ./build-deb.sh
```

## 许可证

[MIT](./LICENSE) （只限本仓库文档，不包含 csnaketerm 上游代码）

csnaketerm 上游代码使用 [GPLv3+](https://github.com/DonaldKellett/csnaketerm/blob/main/LICENSE) 许可证
