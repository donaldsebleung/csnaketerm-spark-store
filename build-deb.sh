#!/bin/bash -e

rm -rf ./dl-assets/
mkdir -p ./dl-assets/
wget -O ./dl-assets/v0.2.0.tar.gz https://github.com/DonaldKellett/csnaketerm/archive/refs/tags/v0.2.0.tar.gz
pushd dl-assets/
tar xvf v0.2.0.tar.gz
pushd csnaketerm-0.2.0/
gzip csnaketerm.6
popd
popd
dpkg-buildpackage -us -uc
lintian ../csnaketerm_0.2.0~spark4_all.deb
