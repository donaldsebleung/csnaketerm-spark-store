经典终端贪食蛇游戏 csnaketerm 是一个以 Node.js 实现的贪食蛇游戏：

- 可选择三个版图：旷野、围墙、迷宫
- 可选择四种难度：初级、中级、高级、疯狂
- 能记录您每个 版图+难度 组合所得的最高分数

详情请参考原 GitHub 仓库：https://github.com/DonaldKellett/csnaketerm

如有问题，请发邮件给 donaldsebleung@qq.com 或于 https://gitee.com/donaldsebleung/csnaketerm-spark-store 建工单。
